import React, { useEffect, useState } from "react";
import twitterLogo from "./assets/twitter-logo.svg";
import "./App.css";

// Constants
const TWITTER_HANDLE = "_buildspace";
const TWITTER_LINK = `https://twitter.com/${TWITTER_HANDLE}`;
const TEST_GIFS = [
  "https://media1.giphy.com/media/jp7jSyjNNz2ansuOS8/200.webp?cid=ecf05e47awchb98frxvqr11713wdwkvn6t84odixdee5vrfl&rid=200.webp&ct=g",
  "https://media2.giphy.com/media/s3d5ugcxFDApG/200w.webp?cid=ecf05e47nqs9j9jnn3fe690jpfgz3jzqgehisfw5noi3mmyz&rid=200w.webp&ct=g",
  "https://media2.giphy.com/media/5goKOyLCarCYrgKeDy/200.webp?cid=ecf05e47xjjoxw8ewmrmz9n10ylu5ylei87sawfxia74p0eg&rid=200.webp&ct=g",
  "https://media0.giphy.com/media/JW9r0SmMeX0TC/200w.webp?cid=ecf05e479zw8nakdwvv6c78s7eoleip8z8xdo8833b3q0hau&rid=200w.webp&ct=g",
];

const App = () => {
  const [walletAddress, setWalletAddress] = useState(null);
  const [inputValue, setInputValue] = useState("");
	const [gifList, setGifList] = useState([])

  const checkIfWalletIsConnected = async () => {
    try {
      const { solana } = window;

      if (solana && solana.isPhantom) {
        console.log("Phantom wallet found!");
        //(XXX) JULIE - see https://docs.phantom.app/integrating/establishing-a-connection#eagerly-connecting for more info
        const response = await solana.connect({ onlyIfTrusted: true });
        const publicKey = response.publicKey.toString();
        console.log("Connected with Public Key:", publicKey);
        setWalletAddress(publicKey);
      } else {
        alert("Solana object not found! Get a Phantom Wallet 👻");
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    const onLoad = async () => {
      await checkIfWalletIsConnected();
    };
    window.addEventListener("load", onLoad);
    return () => window.removeEventListener("load", onLoad);
  }, []);

	useEffect(() => {
		if (walletAddress) {
			console.log('Fetching GIF list...');
			
			// Call Solana program here.
	
			// Set state
			setGifList(TEST_GIFS);
		}
	}, [walletAddress]);


  const connectWallet = async () => {
    const { solana } = window;

    if (solana) {
      const response = await solana.connect();
      console.log("Connected with Public Key:", response.publicKey.toString());
      setWalletAddress(response.publicKey.toString());
    }
  };

  const sendGif = async () => {
    if (inputValue.length > 0) {
      console.log("Gif link:", inputValue);
			setGifList([...gifList, inputValue])
			setInputValue('')
    } else {
      console.log("Empty input. Try again.");
    }
  };

  const onInputChange = (event) => {
    const { value } = event.target;
    setInputValue(value);
  };

  const renderNotConnectedContainer = () => (
    <button
      className="cta-button connect-wallet-button"
      onClick={connectWallet}
    >
      Connect to Wallet
    </button>
  );

  const renderConnectedContainer = () => (
    <div className="connected-container">
      <form
        onSubmit={(event) => {
          event.preventDefault();
          sendGif();
        }}
      >
        <input
          type="text"
          placeholder="Enter gif link!"
          value={inputValue}
          onChange={onInputChange}
        />
        <button type="submit" className="cta-button submit-gif-button">
          Submit
        </button>
      </form>
      <div className="gif-grid">
        {gifList.map((gif) => (
          <div className="gif-item" key={gif}>
            <img src={gif} alt={gif} />
          </div>
        ))}
      </div>
    </div>
  );

  return (
    <div className="App">
      <div className={walletAddress ? "authed-container" : "container"}>
        <div className="header-container">
          <p className="header">🖼 The Ultimate GIF Portal</p>
          <p className="sub-text">
            View your GIF collection in the metaverse ✨
          </p>
        </div>
        {!walletAddress
          ? renderNotConnectedContainer()
          : renderConnectedContainer()}
        <div className="footer-container">
          <img alt="Twitter Logo" className="twitter-logo" src={twitterLogo} />
          <a
            className="footer-text"
            href={TWITTER_LINK}
            target="_blank"
            rel="noreferrer"
          >{`built on @${TWITTER_HANDLE}`}</a>
        </div>
      </div>
    </div>
  );
};

export default App;
